//
//  Sudoku.cpp
//  Sudoku
//
//  Created by Xu Xu on 1/1/14.
//  Copyright (c) 2014 Xu Xu. All rights reserved.
//

#include <iostream>
#include <set>
#include "Sudoku.h"
using namespace std;

Sudoku::Sudoku()
{
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            m_board[i][j] = 0;
        }
    }
}

void Sudoku::setValue(int x, int y, int value)
{
    if (x >= 1 && x <= 9 && y >= 1 && y <= 9 && value >= 0 && value <= 9)
    {
        m_board[x][y] = value;
    }
}

int Sudoku::get(int x, int y)
{
    if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
    {
        return m_board[x][y];
    }
    return -1;
}

void Sudoku::print()
{
    for(int i = 1; i < 10; i++)
    {
        for(int j = 1; j < 10; j++)
        {
            cout << m_board[i][j];
        }
    }
}

bool Sudoku::isValid(bool unique)
{
    // check rows
    for (int i = 1; i < 10; i++)
    {
        set<int> s;
        
        for (int j = 1; j < 10; j++)
        {
            if (m_board[i][j] == 0)
            {
                continue;
            }
            
            if (s.find(m_board[i][j]) != s.end())
            {
                return false;
            }
            
            s.insert(m_board[i][j]);
        }

    }
    
    // check columns
    for (int j = 1; j < 10; j++)
    {
        set<int> s;
        
        for(int i = 1; i < 10; i++)
        {
            if (m_board[i][j] == 0)
            {
                continue;
            }
            
            if (s.find(m_board[i][j]) != s.end())
            {
                return false;
            }
            
            s.insert(m_board[i][j]);
        }
    }
    
    // check Squared
    for(int countj = 1; countj < 10; countj+=3)
    {
        for(int counti = 1; counti < 10; counti+=3)
        {
            set<int> s;
            for(int i = counti; i < counti + 3; i++)
            {
                for (int j = countj; j < countj + 4; j++)
                {
                    if (m_board[i][j] == 0)
                    {
                        continue;
                    }
                    if (s.find(m_board[i][j]) != s.end())
                    {
                        return false;
                    }
                    s.insert(m_board[i][j]);
                }
            }
        }
    }
    return true;

    
}




















