//
//  Sudoku.h
//  Sudoku
//
//  Created by Xu Xu on 1/1/14.
//  Copyright (c) 2014 Xu Xu. All rights reserved.
//

#ifndef __Sudoku__Sudoku__
#define __Sudoku__Sudoku__

class Sudoku
{
public:
    Sudoku();
    
    /**
     @brief: Set the value of the position (x, y)
     @param: x: range: 1 - 9
     @param: y: range: 1 - 9
     @param: value: range: 1 - 9
     */
    void setValue(int x, int y, int value);
    
    /**
     @brief: Get the value of the position (x y)
     @param: x: range: 1 - 9
     @param: y: range: 1 - 9
     @return: range: 1 - 9 or 0: empty, etc.
     */
    int get(int x, int y);
    
    /**
     @brief: Output the Sudoku to console for debug use.
     */
    void print();
    
    /**
     @brief: Check if the Sudoku is valid or not
     @param: unique: if to perform unique solution check
     */
    bool isValid(bool unique);
    
private:
    int m_board[10][10];

};

#endif /* defined(__Sudoku__Sudoku__) */
